# Aurora Saint-Pierre et Miquelon Changelog #

## Update 09 February 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **FIXES files (fix)** : LFVP
- **VOR files (vor)** : LFVP
- **NDB files (ndb)** : LFVP
- **AWY files (hawy/lawy)** : LFVP
- **APT files (apt)** : LFVP
- **STAR files (str)** : LFVP, LFVM
- **Colorscheme (clr)** : FRANCE

---
### Added Files ###

- **Symbols file (sym)** : FR

---
---
## Update 21 June 2024 ##
**{+ AIRAC : 2406 +}**

### Updated Files ###

- **FIXES files (fix)** : LFVP
- **VOR files (vor)** : LFVP
- **NDB files (ndb)** : LFVP
- **AWY files (hawy/lawy)** : LFVP
- **STAR files (str)** : LFVP

---
### Added Files ###

- **ATIS file (atis)** : France

---
---
## Update 22 February 2024 ##
**{+ AIRAC : 2402 +}**

### Updated Files ###

- **FIXES files (fix)** : LFVP
- **VOR files (vor)** : LFVP
- **AWY files (hawy/lawy)** : LFVP

---
---
## Update 21 September 2023 ##
**{+ AIRAC : 2309 +}**

### Updated Files ###

- **FIXES files (fix)** : LFVP
- **VOR files (vor)** : LFVP
- **NDB files (ndb)** : LFVP
- **AWY files (hawy/lawy)** : LFVP
- **APT files (apt)** : LFVP
- **RWY files (rwy)** : LFVP
- **ATC File (atc)** : LFVP
- **SID files (sid)** : LFVP, LFVM
- **STAR files (str)** : LFVP
- **GND files (gnd)** : LFVP, LFVM
- **Taxiway label files (txi)** : LFVP
- **GEO files (geo)** : LFVP

---
### Added Files ###

- **Ground Polygon files (tfl)** : LFVP, LFVM
- **Gates label files (gts)** : LFVP

---
---
## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**No Applicable Updates to date**

---
---
## Update 17 November 2020 ##
**{+ AIRAC : 2012 +}**

**No Applicable Updates to date**